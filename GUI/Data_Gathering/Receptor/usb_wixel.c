/** usb_wixel app:

This app wirelessly receives ADC (Analog-to-Digital Converter) readings from
Wixels running the SensorWixel app, formats them in ASCII, and sends them
to the virtual COM port so they can be displayed or processed on a PC.

You can use this app, along with the SensorWixel app, to wirelessly
read the value of any voltage.  For example, the voltage could be from the
analog output of any of the following sensors, spectrometer or surface scanner.

This app supports having multiple transmitters and multiple receivers. But pay
attention for channel number when programming the Wixels, to be sure you have
just one pair working in the same channel. Valid values are from 0 to 255.

NOTE: The Wixel's ADC inputs can only tolerate voltages between 0 (GND) and
3.3 V.  To measure voltages over a wider range, you will need
additional circuitry (such as a voltage divider).

== Getting Started ==

First, load the SensorWixel app onto one Wixels and connect each
analog voltage output you wish to measure to one of the 6 ADC channels present
on each Wixel: P0_0, P0_1, P0_2, P0_3, P0_4, or P0_5.  Make sure that the ground
(GND) pins of the Wixel and the device producing the analog voltage are connected,
and ensure that everything is sufficiently powered.

Next, load the USBWixel app onto a Wixel and connect it to a
PC via USB.  Open the COM port of that Wixel using a terminal program.
The terminal programs should print (display) a steady stream of lines that have
the following format:

07-37-8B-69 12697   226   226   1501  2931   226   226
 (Serial)   (Time)  (0)   (1)    (2)   (3)   (4)   (5)

(Serial) The serial number of the Wixel that measured the voltages.
(Time)   A 16-bit measure of the time when the receiver processed the report, in
         milliseconds (ms).
(0-5)    The voltages measured on P0_0, P0_1, P0_2, P0_3, P0_4, and P0_5 in
         units of millivolts (mV).

You can also send Commands to digital output pins, these commands have the following format:
  	  D      1      0      1      0
  (Digital) P1_0   P1_1   P1_2   P1_3

*/

/** Dependencies **************************************************************/
#include <wixel.h>
#include <usb.h>
#include <usb_com.h>
#include <radio_queue.h>

#include <stdio.h>

/** Types *********************************************************************/
uint8 led = 0;

typedef struct adcReport
{
    uint8 length;
    uint8 serialNumber[4];
    uint16 readings[6];
} adcReport;

#define BUFFER_SIZE (RADIO_QUEUE_PAYLOAD_SIZE-1)
char buffer[BUFFER_SIZE];

/** Functions *****************************************************************/
void copyXX(uint8 XDATA * dest, uint8 XDATA *src, uint16 size) {
	while(size) {
		*dest++ = *src++;
		size--;
	}
}

void copyPP(uint8 * dest, uint8 *src, uint16 size) {
	while(size) {
		*dest++ = *src++;
		size--;
	}
}

void copyXP(uint8 XDATA * dest, uint8 *src, uint16 size) {
	while(size) {
		*dest++ = *src++;
		size--;
	}
}

void copyPX(uint8 * dest, uint8 XDATA *src, uint16 size) {
	while(size) {
		*dest++ = *src++;
		size--;
	}
}

void updateLeds()
{
    usbShowStatusWithGreenLed();
    //LED_YELLOW(0);
    LED_RED(0);
}

void putchar(char c)
{
    usbComTxSendByte(c);
}

//Read a character
char readchar(){
	return usbComRxReceiveByte();
}

void radioToUsbService()
{
    adcReport XDATA * rxPacket;

    // Check if there is a radio packet to report and space in the
    // USB TX buffers to report it.
    if ((rxPacket = (adcReport XDATA *)radioQueueRxCurrentPacket()) && usbComTxAvailable() >= 64)
    {
        // We received a packet from a Wixel, presumably one running
        // the SensorWixel app.  Format it nicely and send it to
        // the USB host (PC).

        uint8 i;

        printf("%02X-%02X-%02X-%02X %5u",
               rxPacket->serialNumber[3],
               rxPacket->serialNumber[2],
               rxPacket->serialNumber[1],
               rxPacket->serialNumber[0],
               (uint16)getMs()
               );

        for(i = 0; i < 6; i++)
        {
            printf(" %5u", rxPacket->readings[i]);
        }

        putchar('\r');
        putchar('\n');

        radioQueueRxDoneWithPacket();
    }
}

//Read chars from terminal and send via radio
void usbToRadioService(){
	static uint16 lastBufferIndex = 0;

	uint8 XDATA * txPacket;

	if(usbComRxAvailable()){ // If data is available
		if(lastBufferIndex >= BUFFER_SIZE) { //and there is place to put it
			copyPP(&buffer[0],&buffer[1],BUFFER_SIZE-1);
			lastBufferIndex = BUFFER_SIZE - 1;
		}
		buffer[lastBufferIndex] = readchar();

		lastBufferIndex++;
		if(lastBufferIndex == 1 && (buffer[0] == '\n' || buffer[0] == '\r')) {
			// Clear the buffer if only have a \n or \r
			lastBufferIndex = 0;
		}
	}

	// Check to see whether a enter was typed to send a command and
	// whether there is a radio TX buffer available.
	if (lastBufferIndex > 0 && (buffer[lastBufferIndex-1] == '\n' || buffer[lastBufferIndex-1] == '\r') && (txPacket = radioQueueTxCurrentPacket()))
	{
		led ^= 1;
		LED_YELLOW(led);

		// Whether both of those conditions are true, so send a command.

		// Byte 0 is the length.
		txPacket[0] = lastBufferIndex - 1;

		// Copy the command, without the \n
		copyXP(&txPacket[1],buffer,lastBufferIndex-1);
		lastBufferIndex = 0;

		radioQueueTxSendPacket();
	}
}

void main(void)
{
    systemInit();
    usbInit();
    radioQueueInit();

    while(1)
    {
        updateLeds();
        boardService();
        usbComService();
        radioToUsbService();
        usbToRadioService();
    }
}

var express = require('express'),
    sio = require('socket.io'),
	util = require('util'),
	fs = require('fs'),
  exec  = require('child_process').exec;

var child;

var ringBufferLen=256,
    ringBuffer=new ArrayBuffer(ringBufferLen)
	rbI=0,
	rbS=0;
 
app = express.createServer(express.bodyParser(), express.static('public'));
app.listen(80);

function convertBuffer(){
	var result=[];
	var t="";
	var i=rbS;
	while (rbI!=i){
		t=t+String.fromCharCode(ringBuffer[i]);
		i=(i+1)%ringBufferLen;
	}
	var parts=t.split(" ");
	for (var j=0;j<parts.length;++j)
		if (parts[j]!="")
			result.push(parts[j]);
	return result;
}

app.get('/refresh', function(req,res){
	var data="";
	data=req.query.id;
	if (fs.existsSync("public/"+data+".csv"))
		fs.unlinkSync("public/"+data+".csv");
	if (fs.existsSync("public/"+data+".dct.csv"))
		fs.unlinkSync("public/"+data+".dct.csv");
	io.sockets.emit("triggerFlashlight","");
	var body = '';
	res.setHeader('Content-Type', 'text/plain');
	res.setHeader('Content-Length', body.length);
	res.end(body);
});

app.get('/refreshdct', function(req,res){
	var data="";
	data=req.query.id;
	if (fs.existsSync("public/"+data+".dct.csv"))
		fs.unlinkSync("public/"+data+".dct.csv");

	child = exec('/home/user/wes/cpdct public/'+data+'.csv public/'+data+'.dct.csv',
	  function (error, stdout, stderr) {
	    console.log('stdout: ' + stdout);
	    console.log('stderr: ' + stderr);
	    if (error !== null) {
	      console.log('exec error: ' + error);
	    }
	});

	var body = '';
	res.setHeader('Content-Type', 'text/plain');
	res.setHeader('Content-Length', body.length);
	res.end(body);
});

var io = sio.listen(app);
io.sockets.on('connection',function(socket){
	socket.on('sensorprobe',function(data){
		for (var i=0;i<data.length;++i){
			ringBuffer[rbI]=data[i];
			++rbI;
			if (rbI==ringBufferLen)
				rbI=0;
			if (data[i]==10 && ringBuffer[(rbI+ringBufferLen-2)%ringBufferLen]==13)
			{
				rbI=(rbI+ringBufferLen-2)%ringBufferLen;
				var entry=convertBuffer();
				rbS=rbI;
				if (!fs.existsSync("public/"+entry[0]+".csv" ))
					fs.writeFileSync("public/"+entry[0]+".csv" ,"serial,sample,v1,v2,v3,v4,v5\n");
				fs.appendFile("public/"+entry[0]+".csv" ,entry.toString()+"\n");
			}
		}
	});
});
	 
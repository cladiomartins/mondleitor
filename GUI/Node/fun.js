var sio = require('socket.io-client'),
	util = require('util'),
	serialport = require("serialport"),
	capture=false;

var socket = sio.connect("http://localhost");
var SerialPort = serialport.SerialPort; // localize object constructor

var sp = new SerialPort("COM4", {
	baudrate: 9600
});

sp.on('data', function (data) {
	if (capture)
		socket.emit("sensorprobe", data);
});

function triggerFlashlight()
{
	/*
	P1_0; P1_1; P1_2; P1_3
	*/
	setTimeout(function(){
		sp.write("D0000\n");
		sp.write("D0000\n");
		setTimeout(function(){
			sp.write("D1000\n");		
			sp.write("D1000\n");	
			setTimeout(function(){
				sp.write("D0000\n");		
				sp.write("D0000\n");			
				setTimeout(function(){
					sp.write("D0100\n");		
					sp.write("D0100\n");			
					setTimeout(function(){
						sp.write("D0000\n");		
						sp.write("D0000\n");			
						capture=false;
						socket.emit("triggerFlashlightDone","");
					},500);
				},500);
			},500);
		},500);
	},500);
}

socket.on('triggerFlashlight', function(data){
	capture=true;
	triggerFlashlight();
});